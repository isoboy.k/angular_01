export interface BasePost {
  userId: number;
  title: string;
  body: string;
}

export interface Post extends BasePost{
  id: number;
}

export interface CreatePost {
  body: string;
  title: string;
}
