import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { catchError, from, Observable, of, Subject, switchMap, takeUntil } from 'rxjs';

import { Post } from '../../post.model';
import { PostEntityService } from '../../services/post-entity.service';

import { PostEditComponent } from '../post-edit/post-edit.component';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss'],
})
export class PostsListComponent implements OnInit, OnDestroy {
  posts$!: Observable<Post[]>;
  notifier = new Subject()
  constructor(
    private postService: PostEntityService,
    private router: Router,
    private modalService: NgbModal,
    private config: NgbModalConfig,
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  onPostClick(post: Post | null = null): void {


    const postEditModalRef = this.modalService.open(PostEditComponent, { size: 'lg' })
    if (post) {
      postEditModalRef.componentInstance.post = post;
      postEditModalRef.componentInstance.mode = 'update';
    }

    const observable = from(postEditModalRef.result)
        .pipe(
            switchMap(result => {
              return this.postService.changePost(post, result)
            }),
            catchError((reason) => {
              console.log('Dismissed action: ' + reason);
              return of(null)
            })
        )

    observable
        .pipe(
            takeUntil(this.notifier),
        )
        .subscribe()
  }

  ngOnInit(): void {
    this.posts$ = this.postService.entities$;
  }

  ngOnDestroy() {
    this.notifier.next(null)
    this.notifier.complete()
  }
}
