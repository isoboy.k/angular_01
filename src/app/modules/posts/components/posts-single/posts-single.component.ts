import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { map, Observable } from 'rxjs';

import { PostEntityService } from '../../services/post-entity.service';
import { Post } from '../../post.model';


@Component({
  selector: 'app-posts-single',
  templateUrl: './posts-single.component.html',
  styleUrls: ['./posts-single.component.scss']
})
export class PostsSingleComponent implements OnInit {

  public post$!: Observable<Post | undefined>;

  private routeId!: string;
  constructor(
        private postService: PostEntityService,
        private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {

    this.routeId = this.route.snapshot.paramMap.get('id') as string;

    this.post$ = this.postService.entities$
        .pipe(
            map( posts => posts.find(post => post.id.toString() === this.routeId))
        );
  }

}
