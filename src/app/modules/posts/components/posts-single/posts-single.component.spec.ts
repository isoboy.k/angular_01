import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsSingleComponent } from './posts-single.component';
import { PostEntityService } from '../../services/post-entity.service';
import { HttpClientModule } from '@angular/common/http';
import { EntityDataModule } from '@ngrx/data';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

xdescribe('PostsSingleComponent', () => {
  let component: PostsSingleComponent;
  let fixture: ComponentFixture<PostsSingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule, EntityDataModule.forRoot({}), EffectsModule.forRoot([]), StoreModule.forRoot({})],
      providers: [PostEntityService],
      declarations: [ PostsSingleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
