import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Post } from '../../post.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostModalType } from '../../types/post-modal-type';


@Component({
    selector: 'app-post-edit',
    templateUrl: './post-edit.component.html',
    styleUrls: ['./post-edit.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostEditComponent implements OnInit {
    @Input() post!: Post;

    public form: FormGroup;

    public mode: PostModalType = 'create';

    constructor(
        public modal: NgbActiveModal,
        private fb: FormBuilder,
    ) {
        const formControls = {
            body: ['', [Validators.required, Validators.maxLength(450)]],
            title: ['', Validators.required],
        };
        this.form = this.fb.group(formControls);
    }

    ngOnInit() {
        if (this.mode === 'update') {
            this.form.patchValue({...this.post})
        }
    }

}
