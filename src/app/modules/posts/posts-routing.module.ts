import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostsListComponent } from './components/posts-list/posts-list.component';
import { PostsSingleComponent } from './components/posts-single/posts-single.component';
import { PostsComponent } from './components/posts/posts.component';
import { PostsResolver } from './services/posts.resolver';
import { PostEditComponent } from './components/post-edit/post-edit.component';

const routes: Routes = [
    {
        path: '',
        component: PostsComponent,
        resolve: {
            posts: PostsResolver
        },
        children: [
            {
                path: '',
                component: PostsListComponent,
            },
            {
                path: ':id',
                component: PostsSingleComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PostsRoutingModule {
    static components = [
        PostsComponent,
        PostsListComponent,
        PostsSingleComponent,
        PostEditComponent]
}
