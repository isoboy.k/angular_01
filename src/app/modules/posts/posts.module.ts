import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { EntityDataService, EntityDefinitionService, EntityMetadataMap } from '@ngrx/data';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PostsRoutingModule } from './posts-routing.module';
import { PostsDataService } from './services/posts-data.service';
import { PostEntityService } from './services/post-entity.service';
import { PostsResolver } from './services/posts.resolver';
import { ReactiveFormsModule } from '@angular/forms';


const entityMetadata: EntityMetadataMap = {
    Post: {
        entityDispatcherOptions: {
            optimisticUpdate: true
        }
    },
};

@NgModule({
    declarations: PostsRoutingModule.components,
    imports: [
        CommonModule,
        PostsRoutingModule,
        NgbModule,
        ReactiveFormsModule,
    ],
    providers: [
        PostEntityService,
        PostsResolver,
        PostsDataService,
    ],
})
export class PostsModule {
    constructor(
        private eds: EntityDefinitionService,
        private entityDataService: EntityDataService,
        private coursesDataService: PostsDataService,
    ) {
        eds.registerMetadataMap(entityMetadata);
        entityDataService.registerService('Post', coursesDataService);
    }
}
