import { Injectable } from '@angular/core';

import { EntityActionOptions, EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';

import { BasePost, CreatePost, Post } from '../post.model';
import { Observable, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PostEntityService extends EntityCollectionServiceBase<Post> {
  constructor(
      private http: HttpClient,
      serviceElementsFactory: EntityCollectionServiceElementsFactory,
  ) {
    super('Post', serviceElementsFactory);
  }

  override update(entity: Partial<Post>, options?: EntityActionOptions): Observable<Post> {
    return this.http.patch<Post>(`/posts/${entity.id}`, entity)
        .pipe(
            tap(post => {
              super.updateOneInCache(post)
            })
        );
  }

  createPost(entity: BasePost): Observable<Post> {
    return this.http.post<Post>(`/posts/`, entity)
        .pipe(
            tap(post => {
              super.addOneToCache(post);
            })
        );
  }



  changePost(post: Post | null = null, result: CreatePost): Observable<Post> {
      const updatePost = {
          ...post,
          ...result
      }
      const newPost = {
          userId: 1,
          ...result
      }

      return post ? this.update(updatePost) : this.createPost(newPost);
  }
}
