import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { PostEntityService } from './post-entity.service';
import {
    EntityDataModule, EntityDataService,
    EntityMetadataMap
} from '@ngrx/data';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { PostsDataService } from './posts-data.service';
import { BasePost, CreatePost, Post } from '../post.model';


const entityMetadata: EntityMetadataMap = {
    Post: {},
};

describe('PostEntityService', () => {
    let httpClientSpy: jasmine.SpyObj<HttpClient>;
    let service: PostEntityService;

    const basePost: BasePost = {
        userId: 1,
        title: 'Post title',
        body: 'Post body',
    }
    const post: Post = {
        userId: 1,
        title: 'Post title',
        body: 'Post body',
        id: 2,
    }

    const cratePost: CreatePost = {
        title: 'Post title',
        body: 'Post body',
    }
    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);


    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, EffectsModule.forRoot([]), StoreModule.forRoot({}), EntityDataModule.forRoot({entityMetadata: entityMetadata})],
            providers: [
                PostEntityService,
                PostsDataService,
                EntityDataService,
            ],
        });
        service = TestBed.inject(PostEntityService);
    });

    it('should be created', () => {
        let service = TestBed.inject(PostEntityService);

        expect(service).toBeTruthy();
    });

    it('should update', () => {
        let service = TestBed.inject(PostEntityService);
        spyOn(service, 'update').and.returnValue(of(post));
        service.update(post)
        expect(service.update).toHaveBeenCalled();
    });

    it('should createPost', () => {
        let service = TestBed.inject(PostEntityService);
        spyOn(service, 'createPost').and.returnValue(of(post));
        service.createPost(basePost)
        expect(service.createPost).toHaveBeenCalled();
    });

    it('should changePost', () => {
        let service = TestBed.inject(PostEntityService);
        spyOn(service, 'changePost').and.returnValue(of(post));
        service.changePost(post, cratePost)
        expect(service.changePost).toHaveBeenCalled();
    });

    it('should changePost call create', () => {
        let service = TestBed.inject(PostEntityService);
        const changePostSpy = spyOn(service, 'changePost').and.returnValue(of(post));
        service.changePost(null, cratePost);
        expect(changePostSpy).toHaveBeenCalled();
    });

    it('should changePost call update', () => {
        let service = TestBed.inject(PostEntityService);
        const updateSpy = spyOn(service, 'update').and.returnValue(of(post));
        service.changePost(post, cratePost);
        expect(updateSpy).toHaveBeenCalled();
    });
});
