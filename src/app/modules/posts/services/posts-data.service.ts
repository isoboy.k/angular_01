import { Injectable } from '@angular/core';
import { DefaultDataService, DefaultDataServiceConfig, HttpUrlGenerator, QueryParams } from '@ngrx/data';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Post } from '../post.model';

@Injectable()
export class PostsDataService extends DefaultDataService<Post> {

  constructor(
      http: HttpClient,
      httpUrlGenerator: HttpUrlGenerator,
      config: DefaultDataServiceConfig,
  ) {
    super('Post', http, httpUrlGenerator, config);
  }

}
